import React, { useRef, useEffect } from 'react'
import mapboxgl from 'mapbox-gl/dist/mapbox-gl-csp'
import Stats from './Stats'
import { GUI } from './Gui'
// eslint-disable-next-line import/no-webpack-loader-syntax
import MapboxWorker from 'worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker'
import 'threebox-plugin/dist/threebox'

mapboxgl.workerclassName = MapboxWorker
mapboxgl.accessToken =
  'pk.eyJ1IjoibWFwYm94eXkiLCJhIjoiY2ttd3JzZHVyMGh0NTJubjgyNXFnMzBhZCJ9.T7pjJH532gif18aGfzVsGA'

const Object3D = () => {
  const mapContainer = useRef()
  useEffect(() => {
    let styles = {
      day: 'light-v10',
      night: 'dark-v10',
    }
    let selectedStyle = styles.day

    var map = (window.map = new mapboxgl.Map({
      container: mapContainer.current,
      style: 'mapbox://styles/mapbox/' + selectedStyle,
      zoom: 18,
      center: [148.9819, -35.3981],
      pitch: 60,
      antialias: true, // create the gl context with MSAA antialiasing, so custom layers are antialiased
    }))
    // map.on('SelectedFeatureChange', onSelectedFeatureChange)
    function onSelectedFeatureChange(child) {
      console.log('dddddd', child)
    }
    window.tb = new window.Threebox(map, map.getCanvas().getContext('webgl'), {
      realSunlight: true,
      //   sky: true,
      enableSelectingObjects: true,
      enableSelectingFeatures: false, //change this to false to disable fill-extrusion features selection
      enableTooltips: true,
    })

    let model
    // parameters to ensure the model is georeferenced correctly on the map
    var modelOrigin = [148.9819, -35.39847]

    let date = new Date() //new Date(2020, 7, 14, 0, 39); // change this UTC date time to show the shadow view
    let time =
      date.getHours() * 3600 + date.getMinutes() * 60 + date.getSeconds()
    let timeInput = document.getElementById('time')
    let hour = document.getElementById('hour')
    timeInput.value = time
    timeInput.oninput = () => {
      time = +timeInput.value
      date.setHours(Math.floor(time / 60 / 60))
      date.setMinutes(Math.floor(time / 60) % 60)
      date.setSeconds(time % 60)
      map.triggerRepaint()
    }

    function createCustomLayer(layerName, origin) {
      let model
      //create the layer
      let customLayer3D = {
        id: layerName,
        type: 'custom',
        'source-layer': 'building',
        source: 'composite',
        renderingMode: '3d',
        onAdd: function (map, gl) {
          let options = {
            type: 'gltf', //'gltf'/'mtl'
            obj: '/models/Linkhouse.gltf', //model url
            units: 'meters', //units in the default values are always in meters
            scale: 1,
            rotation: { x: 90, y: 180, z: 0 }, //default rotation
            anchor: 'center',
          }
          window.tb.loadObj(options, function (model) {
            model.setCoords(origin)
            model.addTooltip('A radar in the middle of nowhere', true)
            model.traverse((child) => {
              if (child.isMesh) {
                child.addEventListener(
                  'SelectedFeatureChange',
                  onSelectedFeatureChange,
                )
              }
            })

            window.tb.add(model)
            model.castShadow = true
            window.tb.lights.dirLight.target = model
          })
        },
        render: function (gl, matrix) {
          //   window.tb.setSunlight(date, origin) //set Sun light for the given datetime and lnglat
          let dupDate = new Date(date.getTime()) // dup the date to avoid modify the original instance
          let dateTZ = new Date(
            dupDate.toLocaleString('en-US', { timeZone: 'Asia/Bangkok' }),
          )
          //   hour.innerHTML = 'Sunlight on date/time: ' + dateTZ.toLocaleString()
          //   changeStyleWithDaylight(date, origin)
          window.tb.update()
        },
      }
      return customLayer3D
    }
    let stats
    function animate() {
      requestAnimationFrame(animate)
      stats.update()
    }
    map.on('style.load', function () {
      // stats
      stats = new Stats()
      map.getContainer().appendChild(stats.dom)
      animate()

      map.addLayer(createCustomLayer('3d-model', modelOrigin))
    })
    map.on('SelectedChange', onSelectedFeatureChange)
    function onSelectedFeatureChange(e) {
      console.log(e)
    }
    function changeStyleWithDaylight(date, origin) {
      let sunTimes = window.tb.getSunTimes(date, origin)
      if (date >= sunTimes.sunriseEnd && date <= sunTimes.sunsetStart) {
        if (selectedStyle != styles.day) {
          window.tb.setStyle('mapbox://styles/mapbox/' + styles.day)
          selectedStyle = styles.day
        }
      } else {
        if (selectedStyle != styles.night) {
          window.tb.setStyle('mapbox://styles/mapbox/' + styles.night)
          selectedStyle = styles.night
        }
      }
    }
    return () => map.remove()
  }, [])
  return <div className="map-container" ref={mapContainer} />
}

export default Object3D
