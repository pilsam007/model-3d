import './App.css'
// import Map from './Map'
import mapboxgl from 'mapbox-gl/dist/mapbox-gl-csp'
import { useEffect, useRef, useState } from 'react'
// eslint-disable-next-line import/no-webpack-loader-syntax
import MapboxWorker from 'worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker'
import 'threebox-plugin/dist/threebox'
import Stats from './Stats'

mapboxgl.workerClass = MapboxWorker

mapboxgl.accessToken =
  'pk.eyJ1IjoibWFwYm94eXkiLCJhIjoiY2ttd3JzZHVyMGh0NTJubjgyNXFnMzBhZCJ9.T7pjJH532gif18aGfzVsGA'

function App() {
  let origin1 = [-74.001058, 40.714914, 0]
  let origin2 = [-74.0001269, 40.7151698, 50]
  const mapContainer = useRef()

  useEffect(() => {
    const map = new mapboxgl.Map({
      container: mapContainer.current,
      style: 'mapbox://styles/mapbox/outdoors-v11',
      center: origin1,
      zoom: 16.6,
      pitch: 60,
      antialias: true,
      heading: 0,
      hash: true,
    })

    // we can add window. THREEbox to mapbox to add built-in mouseover/mouseout and click behaviors
    window.tb = new window.Threebox(map, map.getCanvas().getContext('webgl'), {
      defaultLights: true,
      enableSelectingFeatures: true, //change this to false to disable fill-extrusion features selection
      enableSelectingObjects: true, //change this to false to disable 3D objects selection
      sky: true,
      enableTooltips: true, // change this to false to disable default tooltips on fill-extrusion and 3D models
    })
    let pointLight = new window.THREE.PointLight('red', 1000, 5000)
    window.tb.add(pointLight)

    let redMaterial = new window.THREE.MeshPhongMaterial({
      color: 0x660000,
      side: window.THREE.DoubleSide,
    })

    let stats
    stats = Stats
    function animate() {
      requestAnimationFrame(animate)
      stats.update()
    }

    let buildings = []
    map.on('style.load', function () {
      //// stats
      stats = new Stats()
      map.getContainer().appendChild(stats.dom)
      animate()

      map.addLayer({
        id: 'custom_layer',
        type: 'custom',
        renderingMode: '3d',
        onAdd: function (map, mbxContext) {
          //[jscastro] we add a star based on this example https://window. THREEjs.org/examples/?q=extrud#webgl_geometry_extrude_shapes
          const points = [],
            numPts = 5
          for (let i = 0; i < numPts * 2; i++) {
            const l = i % 2 == 1 ? 10 : 20
            const a = (i / numPts) * Math.PI
            points.push(
              new window.THREE.Vector2(Math.cos(a) * l, Math.sin(a) * l),
            )
          }
          const material1 = new window.THREE.MeshLambertMaterial({
            color: 0xb00000,
            wireframe: false,
          })
          const material2 = new window.THREE.MeshLambertMaterial({
            color: 0xff8000,
            wireframe: false,
          })
          let star = window.tb.extrusion({
            coordinates: points,
            geometryOptions: {
              depth: 20,
              steps: 1,
              bevelEnabled: true,
              bevelThickness: 2,
              bevelSize: 4,
              bevelSegments: 1,
            },
            anchor: 'center',
            units: 'meters',
            rotation: { x: 90, y: 0, z: 20 },
            materials: [material1, material2],
          })
          star.addTooltip('A animated extruded star over Columbus Park', true)
          star.setCoords(origin2)
          star.set({ rotation: { x: 0, y: 0, z: 720 }, duration: 20000 })
          window.tb.add(star)

          window.d3.json('/geojson/extrusion.geojson').then(function (fc) {
            console.log(fc)
            //then we create the extrusions based on the geoJson features
            addBuildings(fc.features)
          })

          // console.log(star, material1, material2)
        },
        render: function (gl, matrix) {
          window.tb.update()
        },
      })
    })
    function addBuildings(data, info, height = 1) {
      data.forEach((b) => {
        let center = b.properties.center
        let s = window.tb.projectedUnitsPerMeter(center[1])

        let extrusion = window.tb.extrusion({
          coordinates: b.geometry.coordinates,
          geometryOptions: {
            curveSegments: 1,
            bevelEnabled: false,
            depth: b.layer.paint['fill-extrusion-height'] * s,
          },
          materials: redMaterial,
        })
        extrusion.addTooltip(b.properties.tooltip, true)
        extrusion.setCoords([center[0], center[1], 0])
        window.tb.add(extrusion)
      })
    }
    return () => map.remove()
  }, [])

  return (
    <div>
      <div className="map-container" ref={mapContainer} />
    </div>
  )

  // return <Map />
}

export default App
