import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import Map from './Map'
import Object3D from './3dObject'
import FloorPlan from './FloorPlan'
import reportWebVitals from './reportWebVitals'

ReactDOM.render(
  <React.StrictMode>
    <Object3D />
  </React.StrictMode>,
  document.getElementById('root'),
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
