import React, { useRef, useEffect } from 'react'
import mapboxgl from 'mapbox-gl/dist/mapbox-gl-csp'
import Stats from './Stats'
import { GUI } from './Gui'
// eslint-disable-next-line import/no-webpack-loader-syntax
import MapboxWorker from 'worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker'
import 'threebox-plugin/dist/threebox'

mapboxgl.workerclassName = MapboxWorker
mapboxgl.accessToken =
  'pk.eyJ1IjoibWFwYm94eXkiLCJhIjoiY2ttd3JzZHVyMGh0NTJubjgyNXFnMzBhZCJ9.T7pjJH532gif18aGfzVsGA'

const Map = () => {
  const mapContainer = useRef()

  useEffect(() => {
    //starting location for map
    let popup
    let minZoom = 12
    let mapConfig = {
      ALL: {
        center: [-73.985699, 40.750042, 0],
        zoom: 16.25,
        pitch: 45,
        bearing: 0,
      },
      names: {
        compositeSource: 'composite',
        compositeSourceLayer: 'building',
        compositeLayer: '3d-buildings',
      },
    }

    let map = new mapboxgl.Map({
      container: mapContainer.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: mapConfig.ALL.center,
      zoom: mapConfig.ALL.zoom,
      pitch: mapConfig.ALL.pitch,
      bearing: mapConfig.ALL.bearing,
      antialias: true,
      hash: true,
    })

    // we can add window. THREEbox to mapbox to add built-in mouseover/mouseout and click behaviors
    window.tb = new window.Threebox(map, map.getCanvas().getContext('webgl'), {
      defaultLights: true,
      enableSelectingFeatures: true, //change this to false to disable fill-extrusion features selection
      enableTooltips: true, // change this to false to disable default tooltips on fill-extrusion and 3D models
    })

    let stats, gui
    function animate() {
      requestAnimationFrame(animate)
      stats.update()
    }

    let api = {
      fov: (Math.atan(3 / 4) * 180) / Math.PI,
    }
    function init() {
      // stats
      stats = new Stats()
      map.getContainer().appendChild(stats.dom)
      animate()
      // gui
      gui = new GUI()
      // going below 2.5 degrees will start to generate serious issues with polygons in fill-extrusions and 3D meshes
      // going above 45 degrees will also produce clipping and performance issues
      gui.add(api, 'fov', 2.5, 45.0).step(0.1).onChange(changeFOV)
    }

    function changeFOV() {
      window.tb.fov = api.fov
    }

    map.on('style.load', function () {
      init()
      map.addLayer(createCompositeLayer())

      //[jscastro] there is no need to add a layer if we are not adding 3D objects or models, we can subscribe to render to invoke update
      map.on('render', function () {
        window.tb.update()
      })

      //[jscastro] subscribe to selected/unselect extrusion feature event
      map.on('SelectedFeatureChange', onSelectedFeatureChange)
    })

    function createCompositeLayer() {
      let layer = {
        id: mapConfig.names.compositeLayer,
        source: mapConfig.names.compositeSource,
        'source-layer': mapConfig.names.compositeSourceLayer,
        filter: ['==', 'extrude', 'true'],
        type: 'fill-extrusion',
        minzoom: minZoom,
        paint: {
          'fill-extrusion-color': [
            'case',
            ['boolean', ['feature-state', 'select'], false],
            'lightgreen',
            ['boolean', ['feature-state', 'hover'], false],
            'lightblue',
            '#aaa',
          ],

          // use an 'interpolate' expression to add a smooth transition effect to the
          // buildings as the user zooms in
          'fill-extrusion-height': [
            'interpolate',
            ['linear'],
            ['zoom'],
            minZoom,
            0,
            minZoom + 0.05,
            ['get', 'height'],
          ],
          'fill-extrusion-base': [
            'interpolate',
            ['linear'],
            ['zoom'],
            minZoom,
            0,
            minZoom + 0.05,
            ['get', 'min_height'],
          ],
          'fill-extrusion-opacity': 0.9,
        },
      }
      return layer
    }

    //[jscastro] handler for the feature selected UI... we will create a Mapbox popup to compare with Threebox popup
    function onSelectedFeatureChange(e) {
      console.log(e)
      let feature = e.detail
      if (feature && feature.state && feature.state.select) {
        if (popup) popup.remove()

        let center = []
        let coords = window.tb.getFeatureCenter(feature, null, 0)

        center.push([coords[0], coords[1]])

        //TODO: this creates a Mapbox popup in the same coords the Threebox popup to see the difference
        popup = new mapboxgl.Popup({ offset: 0 })
          .setLngLat(center[0].slice())
          .setHTML('<strong>' + feature.properties.height + '</strong >')
          .addTo(map)

        let geoJson = {
          geometry: feature.geometry,
          type: 'Feature',
          properties: feature.properties,
        }
        // console.log(JSON.stringify(geoJson, null, 2))
      }
    }

    return () => map.remove()
  }, [])

  return <div className="map-container" ref={mapContainer} />
}

export default Map
