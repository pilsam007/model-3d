import React, { useRef, useEffect } from 'react'
import mapboxgl from 'mapbox-gl/dist/mapbox-gl-csp'
import Stats from './Stats'
// eslint-disable-next-line import/no-webpack-loader-syntax
import MapboxWorker from 'worker-loader!mapbox-gl/dist/mapbox-gl-csp-worker'
import 'threebox-plugin/dist/threebox'

mapboxgl.workerclassName = MapboxWorker
mapboxgl.accessToken =
  'pk.eyJ1IjoibWFwYm94eXkiLCJhIjoiY2ttd3JzZHVyMGh0NTJubjgyNXFnMzBhZCJ9.T7pjJH532gif18aGfzVsGA'

const FloorPlan = () => {
  const mapContainer = useRef()
  let tb
  let origin = [-87.61694, 41.86625]

  useEffect(() => {
    var map = new mapboxgl.Map({
      container: mapContainer.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: origin,
      zoom: 15.99,
      pitch: 40,
      bearing: 20,
      antialias: true,
      hash: true,
    })

    window.tb = new window.Threebox(map, map.getCanvas().getContext('webgl'), {
      defaultLights: true,
      enableSelectingObjects: true,
      enableDraggingObjects: true,
      enableTooltips: true,
      multiLayer: true, // this will create a default custom layer that will manage a single tb.update
    })
    let stats
    map.on('load', function () {
      // stats
      stats = new Stats()
      map.getContainer().appendChild(stats.dom)
      animate()

      map.addLayer(createExtrusionLayer('room-extrusion', 'floor-plan'))

      for (let i = 0; i < 5; i++) {
        map.addLayer(createCustomLayer('customLayer' + i, origin, i))
      }
      map.addLayer(
        createCustomLayer('customLayer' + 5, [-87.617299, 41.865728], 0),
      )
    })
    function animate() {
      requestAnimationFrame(animate)
      stats.update()
    }
    function onSelectedFeatureChange(e) {
      console.log('ddd', e)
    }
    function createExtrusionLayer(layerId, sourceId, i) {
      map.addSource(sourceId, {
        // GeoJSON Data source used in vector tiles, documented at
        // https://gist.github.com/ryanbaumann/a7d970386ce59d11c16278b90dde094d
        type: 'geojson',
        data: '/geojson/3dMap.geojson',
      })
      let extrusionLayer = {
        id: layerId,
        type: 'fill-extrusion',
        source: sourceId,
        'source-layer': 'building',
        paint: {
          // See the Mapbox Style Specification for details on data expressions.
          // https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions

          // Get the fill-extrusion-color from the source 'color' property.
          'fill-extrusion-color': ['get', 'color'],

          // Get fill-extrusion-height from the source 'height' property.
          'fill-extrusion-height': ['get', 'height'],

          // Get fill-extrusion-base from the source 'base_height' property.
          'fill-extrusion-base': ['get', 'base_height'],

          // Make extrusions slightly opaque for see through indoor walls.
          'fill-extrusion-opacity': 1,
        },
      }
      return extrusionLayer
    }

    function createCustomLayer(layerId, origin, i) {
      //create the layer
      let customLayer3D = {
        id: layerId,
        type: 'custom',
        renderingMode: '3d',
        onAdd: function (map, gl) {
          addModel(layerId, origin, i)
        },
        render: function (gl, matrix) {
          // is not needed anymore if multiLayer : true
        },
      }
      return customLayer3D
    }

    function addModel(layerId, origin, i) {
      let options = {
        type: 'gltf', //'gltf'/'mtl'
        obj: '/models/Linkhouse.gltf', //model url
        bin: '', //replace by mtl attribute
        units: 'meters', //units in the default values are always in meters
        scale: 0.16,
        rotation: { x: 90, y: 180, z: 0 }, //default rotation
        anchor: 'center',
      }
      window.tb.loadObj(options, function (model) {
        model.setCoords([origin[0] + i * 0.00012, origin[1]], 0)
        let l = map.getLayer(layerId)
        model.traverse((child) => {
          if (child.isMesh) {
          }
        })
        window.tb.add(model, layerId)
      })
    }
    return () => {
      map.remove()
    }
  }, [])
  return <div className="map-container" ref={mapContainer} />
}

export default FloorPlan
